﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class InputManager : MonoBehaviour
{
    [SerializeField] Player player;
 
    Vector2 input;
 
    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        input.x = Input.GetAxis("Horizontal");
        //input.y = Input.GetAxis("Vertical");
 
        player.SetAxis(input.x);
 
        if (Input.GetButtonDown("Jump"))
        {
            player.JumpStart();
        }
    }
}