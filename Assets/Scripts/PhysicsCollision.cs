﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
[RequireComponent(typeof(Rigidbody))]
public class PhysicsCollision : MonoBehaviour
{
    [Header("Physics")]
    public float gravityScale = 1;
    protected Rigidbody rb;
    [Header("Collisions")]
    public bool isGrounded;
    public bool wasGrounded;
    public bool justGrounded;
    public bool justNotGrounded;
    public bool isTouchingWall;
    public bool isFacingRight;
    [Header("Ground Checker")]
    public float distToGround;
    public LayerMask groundMask;
    public float rayOffseGround;
    public int maxRayGround;
    protected Vector3 rayGroundDir = Vector3.down;
    [Header("Wall Checker")]
    public float distToWall;
    public float rayOffseWall;
    public int maxRayWall;
    protected Vector3 rayWallDir = Vector3.right;
 
    protected virtual void Start()
    {
        isFacingRight = true;
        rb = GetComponent<Rigidbody>();
        rb.useGravity = false;
    }
 
    protected virtual void FixedUpdate()
    {
        rb.AddForce(Vector3.up * Physics.gravity.y * gravityScale, ForceMode.Acceleration);
 
        CheckGround();
        CheckWall();
    }
 
    void CheckGround()
    {
        wasGrounded = isGrounded;
        isGrounded = false;
        justGrounded = false;
        justNotGrounded = false;
 
        Vector3 rayOrigin = Vector3.zero;
        int mult = 0;
 
        for(int i = 0; i < maxRayGround; ++i)
        {
            RaycastHit hit = new RaycastHit();
            if(Physics.Raycast(rayOrigin + transform.position, rayGroundDir,out hit, distToGround, groundMask))
            {
                if(hit.normal.y > 0.8f)
                {
                    isGrounded = true;
                    if(!wasGrounded) justGrounded = true;
                    break;
                }                
            }
 
            if(mult <= 0)
            {
                mult *= -1;
                mult++;
            }
            else mult *= -1;
 
            rayOrigin.x = mult * rayOffseGround;
        }
 
        if(wasGrounded && !isGrounded) justNotGrounded = true;
    }
    void CheckWall()
    {
        isTouchingWall = false;
 
        Vector3 rayOrigin = Vector3.zero;
        int mult = 0;
 
        for(int i = 0; i < maxRayWall; ++i)
        {
            RaycastHit hit = new RaycastHit();
            if(Physics.Raycast(rayOrigin + transform.position, rayWallDir, out hit, distToWall, groundMask))
            {
                if(Mathf.Abs(hit.normal.x) > 0.85f)
                {
                    isTouchingWall = true;
                    break;
                }
            }
            if(mult <= 0)
            {
                mult *= -1;
                mult++;
            }
            else mult *= -1;
 
            rayOrigin.y = mult * rayOffseWall;
        }
    }
 
    protected virtual void Flip()
    {
        isFacingRight = !isFacingRight;
        if(isFacingRight) rayWallDir = Vector3.right;
        else rayWallDir = Vector3.left;
    }
 
    void OnDrawGizmos()
    {
        DrawGroundRays();
        DrawWallRays();
    }
    void DrawGroundRays()
    {
        if(isGrounded) Gizmos.color = Color.red;
        else Gizmos.color = Color.blue;
 
        Vector3 rayOrigin = Vector3.zero;
        int mult = 0;
 
        for(int i = 0; i < maxRayGround; ++i)
        {
            Gizmos.DrawRay(rayOrigin + transform.position, rayGroundDir * distToGround);
 
            if(mult <= 0)
            {
                mult *= -1;
                mult++;
            }
            else mult *= -1;
 
            rayOrigin.x = mult * rayOffseGround;
        }
    }
    void DrawWallRays()
    {
        if(isTouchingWall) Gizmos.color = Color.red;
        else Gizmos.color = Color.blue;
 
        Vector3 rayOrigin = Vector3.zero;
        int mult = 0;
 
        for(int i = 0; i < maxRayWall; ++i)
        {
            Gizmos.DrawRay(rayOrigin + transform.position, rayWallDir * distToWall);
 
            if(mult <= 0)
            {
                mult *= -1;
                mult++;
            }
            else mult *= -1;
 
            rayOrigin.y = mult * rayOffseWall;
        }
    }
 
 
 
 
}